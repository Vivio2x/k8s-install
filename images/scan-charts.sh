#!/bin/bash
TOP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )"
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
CHARTS_DIR=$TOP_DIR/components/charts
DEFAULT_NAME_NULL="default_name_null"
IMAGES=""

add_image() {
  echo $1 >> $CUR_DIR/images.txt.tmpl
}

format_image() {
  local prefix=""
  local image_name=$1
  local format_name=""
  case ${image_name%%/*} in
    "")
      prefix=docker.io
      format_name=$prefix/${image_name#*/}
      ;;
    *.io|*.com)
      #echo "normal"
      format_name=$image_name
      ;;
    *)
      prefix=docker.io
      format_name=$prefix/$image_name
      ;;  
  esac
  add_image $format_name
  echo $format_name
}


get_image_info() {
  #set -x
  local file=$1
  local obj=$2
  local name=$3  
  local tags="tag version"
  local t=""
  for t in $tags; do
    #echo title=$t
    local check=$(yq ".$obj| has(\"$t\")" $file)
    if $check; then
      local tag=$(yq .$obj.$t $file)
      break
    fi
  done
  if [ -z "$tag" ]; then
    tag=latest
  fi


  local regs="registry"
  local r=""
  for r in $regs; do 
    #echo title=$r
    local check=$(yq ".$obj| has(\"$r\")" $file)
    if $check; then
      local registry=$(yq .$obj.$r $file)
      break
    fi
  done
 
  local images="image repository name"
  for img in $images; do
    local check=$(yq ".$obj| has(\"$img\")" $file)
    if $check; then
      local image=$(yq .$obj.$img $file)
      break
    fi
  done

  if [ -n "$image" ] ; then
    if [ -z "$registry" ]; then
      echo $(format_image $image:$tag)
    else
      echo $(format_image $registry/$image:$tag)
    fi
  elif [ "$name" == "DEFAULT_NULL" ]; then
    echo $(format_image $registry/$name:$tag)
  else
    echo ""
  fi
}

OBJ_TMP="-x-"
get_image_from_yaml_by_name() {
  local f=$1
  local str=$2
  local key=$3
  local tmp=$4
  local name=${str##*$key}
  local obj=${str%%$key*}
  if [ "$obj" == "$OBJ_TMP" ] || [ -z "$obj" ]; then
    echo "obj=$obj, tmp=$OBJ_TMP, continue"
    return 1
  fi
  OBJ_TMP=$obj
  if [[ "$name" =~ ":" ]]; then
    echo "$(format_image $name) "
  elif [ -z "$name" ]; then
    echo "invalid str $j"
    return 1
  else
    echo "$(get_image_info $f $obj $name) "
    #break
  fi
  return 0
}

get_image_from_yaml_by_title() {
  local f=$1
  local str=$2
  local title=$3
  local obj=${str%%.$title.*}.$title
  if [ "$obj" == "$OBJ_TMP" ] || [ -z "$obj" ]; then
    #echo "obj=$obj, tmp=$OBJ_TMP, continue"
    return
  fi
  OBJ_TMP=$obj
  echo "$(get_image_info $f $obj $DEFAULT_NAME_NULL) "
}

scan_file() {
  local f=$1
  local name=""
  local obj=""
  OBJ_TMP="-x-"
  str=$(yq . -o props $f | grep -E "\.image[.| ]|^image\." | sed "s/ = /=/g")
    for j in $str; do
      case $j in
        *\.image\.*) 
	  
          get_image_from_yaml_by_title $f $j "image"
	  ;;

        *\.image=*)
          get_image_from_yaml_by_name $f $j ".image="
          ;;
    
        image.*)	  
          if [[ "$j" =~ ".repository=" ]]; then
            get_image_from_yaml_by_name $f $j ".repository="
          elif [[ "$j" =~ ".name=" ]]; then
	    get_image_from_yaml_by_name $f $j ".name="
	  fi
	  ;;

        *)
          echo "unknown: $j"
          ;;	  
      esac
    done
}

if [ -n "$1" ]; then
  scan_file $1
  exit
fi

infos=$(find $CHARTS_DIR -name "*.yaml" -o -name "*.yml" |  grep -vE "/crds/|/crd/|crd.yaml|crds.yaml" | xargs grep -rn "image:" | grep -v "{{")
files=$(echo "$infos" | awk -F ":" '{print $1}' | uniq)

#echo "files=$files"
#echo "=============================================="
#exit

for i in $files; do
  chartPath=${i#*charts/}
  chartName=${chartPath%/*}
  echo -------------------${chartName}
  scan_file $i 
done

cat $CUR_DIR/images.txt.tmpl | uniq >>  $CUR_DIR/images-charts.txt
rm -rf $CUR_DIR/images.txt.tmpl 
