#!/bin/bash
#TOP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." &> /dev/null && pwd )"
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ -f /etc/docker/registry/config.yml ]; then
  DATA_PATH=$(yq .storage.filesystem.rootdirectory /etc/docker/registry/config.yml)
  
  set -x
  tar Czxvf ${DATA_PATH} $CUR_DIR/docker.tar.gz > /dev/null
fi

