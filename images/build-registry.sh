#!/bin/bash
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
MIRROS=registry.aliyuncs.com/google_containers


defaultLocalRegistry=registry.k8s.local:5000
defaultRemoteRegistry="" #registry.vivio-sz.com
defaultImageFiles=$CUR_DIR/images.txt
defaultScanCharts=false

pyellow() {
  echo -e "\033[33m $1 \033[0m"
}

pblue() {
  echo -e "\033[34m $1 \033[0m"
}

printHelp() {
  cat <<EOF
Script to install docker and containerd on genesis node

Usage: $0
  --remote        <string> or <IP>:<port>, remote registry 
  --local         <string> or <IP>:<port>, local registry         
  --scan-charts    scan all charts to get name list of image
  --image-file    filename of image list, multi files use ',' to split.
  --help          print help message.
example: $0 --remote=registry.vivio-sz.com --local=registry.k8s.local --image-file=images-kube.txt,images-charts.txt
EOF
}

read_input() {
  while [ $# -gt 0 ]; do
    case "$1" in
    --remote=*)
      remoteRegistry="${1#*=}"

      ;;

    --local=*)
      localRegistry="${1#*=}"
      ;;

    --image-file=*)
      imageFiles="${1#*=}"
      ;;
    
    --scan-charts)
      scanCharts=true
      ;;

    --help)
      printHelp
      exit 0
      ;;

    *)
      pyellow "Unknown flag: $1"
      echo ""
      printHelp
      exit 0
      ;;
    esac
    shift
  done

  localRegistry=${localRegistry:-$defaultLocalRegistry}
  remoteRegistry=${remoteRegistry:-$defaultRemoteRegistry}
  imageFiles=${imageFiles:-$defaultImageFiles}
  scanCharts=${scanCharts:-$defaultScanCharts}
}


get_image_mirror() {
  local image=$1
  local io=${image%%/*}

  case $io in
    k8s.gcr.io) 
      local name=${image##*/}
      echo "$MIRROS/$name" ;;
    *) echo "$image"
  esac
}


#if systemctl is-active docker-registry; then
#  systemctl stop docker-registry
#fi
read_input "$@"

echo "localregistry=$localRegistry remoteRegistry=$remoteRegistry"

if $scanCharts ; then
  pblue "scan charts to get name list of images"
  bash $CUR_DIR/scan-charts.sh
  imageFiles="$imageFiles,$CUR_DIR/images-charts.txt"
fi

set -x; docker login $localRegistry -u admin -p Harbor12345; set +x

files=$(echo $imageFiles | sed 's/,/ /g')
for i in $files; do
  lines=$(cat $i | grep -vE "^#|^[ ]*$")
  count=$(echo "$lines" | wc -l)
  cnt=0
  while read line; do
    if [ -z "$(echo $line | grep -vE '^#|^[ ]*$')" ]; then
      continue
    fi
    let cnt=cnt+1 
    #set -x
    localImage=${line%@*}
    if [ -z "$remoteRegistry" ]; then
      remoteImage=${line#*@}
      remoteTargetImage=$(get_image_mirror $remoteImage)
    else
      remoteTargetImage=$remoteRegistry/${localImage#*/}
    fi
    localTargetImage=$localRegistry/${localImage#*/}
    echo $(pyellow $cnt/$count): $(pblue "pull $remoteTargetImage, push $localTargetImage")
    set -x 
    docker pull $remoteTargetImage
    docker tag $remoteTargetImage $localTargetImage
    docker push $localTargetImage
    set +x
    echo "    target=$remoteTargetImage [ok]"
  done < $i
done


docker logout $localRegistry 

