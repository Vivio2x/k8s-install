#!/bin/bash

TOP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )"
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ -n "$1" ]; then
    args="--version=$1"
fi

bash -xc "$CUR_DIR/install.sh -fd $args"
