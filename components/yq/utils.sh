#!/bin/bash
MINIO_SERVER=${MINIO_SERVER:-192.168.49.15:9000}
VERSION_REGEX='[0-9]+\.([0-9]+)\.([0-9]+)'
PACKAGE_MANAGER=$((command -v apt-get > /dev/null && echo apt-get) \
    || (command -v yum > /dev/null && echo yum) \
    || (echo "Neither apt-get nor yum is installed, please install one of them first." && exit 1))    
# Check if minio command is installed and configured correctly
if [ -z "$(command -v mc &> /dev/null && mc config host list | grep "$MINIO_SERVER")" ]; then
  MINIO_ENABLE=false
else
  MINIO_ENABLE=true
fi

FORCE=${FORCE:-false}
# 检查是否有curl和wget
HAS_CURL="$(command -v curl >/dev/null 2>&1 && echo true || echo false)"
HAS_WGET="$(command -v curl >/dev/null 2>&1 && echo true || echo false)"
# 检查本机的软件管理工具是用apt-get还是yum
HAS_APT="$(command -v apt-get >/dev/null 2>&1 && echo true || echo false)"
HAS_YUM="$(command -v yum >/dev/null 2>&1 && echo true || echo false)"

bash_xc='bash -xc'

if ! $HAS_CURL && ! $HAS_WGET; then
  echo "Either curl or wget is required"
  exit 1
else
  $HAS_CURL && echo "curl: $(curl --version | head -n 1 | awk '{print $2}')"
  $HAS_WGET && echo "wget: $(wget --version | head -n 1 | awk '{print $3}')"
fi

#usage: download_url <url> [target] 
download_url() {
  local target
  local url=$1
  local remote_file=""
  
  if [ $# -eq 2 ]; then
    target=$2
  elif [ $# -eq 1 ]; then 
    target=${1##*/}
  else
    echo "cmd err, usage: download_url <url> [target]"
    return 1
  fi
  if [ ! -f "$target" ] || ${FORCE} ; then
    if $MINIO_ENABLE && [ "$(echo $url | awk -F[/] '{print $3}')" == $MINIO_SERVER ] ; then
      remote_file=${url##*$MINIO_SERVER/}
      $bash_xc "mc cp minio/$remote_file $target"
    elif $HAS_CURL; then
      $bash_xc "curl -fsSL $url -o $target"
      if [ $? -ne 0 ]; then
	      echo "curl or wget failed"
        return 3
      fi
    elif $HAS_WGET; then
      $bash_xc "wget -q $url -O $target"
      if [ $? -ne 0 ]; then
	      echo "curl or wget failed"
        return 3
      fi
    fi
    echo "get $(basename $target) successfully"
    return 0
  else
    echo "do not get $(basename $url): file exist or force=${FORCE} "
    return 0
  fi
  return 0
}

download_url_until_succeed() {
  local target
  local url=$1
  
  if [ $# -eq 2 ]; then
    target=$2
  elif [ $# -eq 1 ]; then 
    target=${1##*/}
  else
    echo "cmd err, usage: download_url <url> [target]"
    return 1
  fi
  echo "download $url $target"
  
  while ! download_url $url $target; do
    sleep 5
  done
}

clean_locks() {
    if $HAS_APT; then
        rm -rf /var/lib/dpkg/lock
        rm -rf /var/lib/dpkg/lock-frontend
        rm -rf /var/cache/apt/archives/lock
    elif $HAS_YUM; then
        rm -rf /var/run/yum.pid
        rm -rf /var/run/yum.pid.old
        # rm -rf /var/cache/yum/*
    fi
}

# check if the current version is greater than or equal to the target version.
# 检查当前版本是否大于或等于目标版本。
check_version_ge() {
    test "$(echo "$@" | tr " " "\n" | sort -rV | head -n 1)" == "$1"
}

# get the latest version of packages.
# usage: get_latest_version <pkg_name>
# 获取组件的最新版本。
# 使用方法： get_latest_version <软件包名称>
get_latest_version() {
    local pkg_name=$1
    if $HAS_APT; then
        echo $(apt-cache madison $pkg_name | head -1 | grep -Eo "$VERSION_REGEX")
    elif $HAS_YUM; then
        echo $(yum list $pkg_name --showduplicates | grep -Eo "$VERSION_REGEX" | sort -rV | head -1)
    fi
}


# delete packages.
# usage: delete_pkg <pkg_name>
# 删除软件包。
# 使用方法： delete_pkg <软件包名称>
delete_pkg() {
    local pkg_name=$1
    if $HAS_APT; then
        local pkgs=$(dpkg -l | grep -E "$pkg_name" | awk '{print $2}')
        if [ -n "$pkgs" ] ; then
            set -x
            apt-get remove -y $pkgs > /dev/null 2>&1
            apt-get autoremove -y > /dev/null
            apt-get autoclean -y > /dev/null
            set +x
        fi
    elif $HAS_YUM; then
        local pkgs=$(rpm -qa | grep -E 'helm' | awk '{print $1}')
        if [ -n "$pkgs" ] ; then
            set -x
            yum remove -y $pkgs > /dev/null 2>&1
            yum autoremove -y > /dev/null
            yum clean all > /dev/null
            set +x
        fi
    fi
}