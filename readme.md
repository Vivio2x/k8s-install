[toc]{type: "ul", level: [1,2]}

# 1. 简介

k8s安装，适用于ubuntu/centos系统

**特别说明：**

安装高可用集群，如果要支持VIP漂移，必须包含奇数个master节点！！！

安装高可用集群，如果要支持VIP漂移，必须包含奇数个master节点！！！

安装高可用集群，如果要支持VIP漂移，必须包含奇数个master节点！！！



## 1.1. 目录说明

```bash
install/
├── components # 组件目录
│   ├── charts # 应用包chart目录，包含所有chart的安装
│   ├── conntrack # 必备软件conntrack
│   ├── docker    # docker和containerd的安装
│   ├── ebtables  # 必备软件ebtables
│   ├── ethtool   # 必备软件ethtool
│   ├── kube-vip  # 虚拟ip, 用于高可用
│   ├── kubeadm   # kubeadm/kubelet/kubectl的安装
|   ├── helm      # helm安装
│   └── yq        # yaml文件读取和修改的小工具
├── config        # 部署集群时需要的配置
│   ├── create-config.sh  # 创建配置
│   ├── etc               # 部署集群时需要放到/etc/目录下的文件
│   ├── package-1.21.11.yaml # 安装包说明，定义需要安装哪些组件
│   └── template          # 配置模板
├── images        # 镜像包，未完成
├── k8s-install.sh # 部署脚本
├── readme.md     # 使用说明
└── scripts       # 脚本
    ├── k8s-env.sh  # 基础环境设置
    ├── k8s-pkg-build.sh # 打包脚本，可用于构造离线安装包
    ├── k8s-rm.sh    # 卸载集群
    ├── k8s-start.sh # 启动集群
    ├── k8s-stop.sh  # 终止集群
    └── utils.sh     # 库函数
```

## 1.2. 安装命令说明

```bash
Usage: ./k8s-install.sh [OPTIONS]
Options:
  -o|--online             Set installation mode to online#使用apt或者yum安装, 不指定则使用二进制包安装
  -f|--force              Set installation force to true#强制覆盖安装所有软件
  -s|--startnow           Set start now to true#安装后立即启动
  -d|--downloadonly       Download only #构建离线安装包（二进制安装包）
  -y|--yes                Automatically input y or yes#安装过程中遇到yes/y,自动输入y
  --master=<a.b.c.d>      Set the IP address of the master node#设置master(apiserver) IP，如果IP不是本机IP，则加入集群，如果IP是本机IP，则创建集群
  --rm|--remove           Uninstall kubernetes software, including docker#卸载所有软件，包括docker
  --ha                    High Availability Enable # 是否为高可用集群
  -c|--clean              Remove installation package #删除安装包
  --help                  Show this help message and exit
```

例：

```bash
#在线安装，创建集群，安装完成后立即启动
./k8s-install.sh --online --startnow

#在线安装，创建集群，安装完成后，手动启动
./k8s-install.sh --online 
k8s-start

#在线安装，创建集群，强制安装配置中指定的软件版本, 并自动输入yes
./k8s-install.sh -ofsy

#在线安装，加入集群, 指定master(apiServer，即第一个节点)的ip为10.200.8.110
#如果10.200.8.110是本机IP，则自动创建新集群
./k8s-install.sh -osy --master=10.200.8.110
```

## 1.3. 快速安装 [在线安装]

例如：假设有多台主机，其中一台主机`10.200.8.110`做为master节点。则部署过程如下：

第一个节点：创建新集群。复制以下内容并**修改其中的配置**，在节点终端上执行。

```bash
#下载代码，如果安装git就使用git && cd, 否则使用wget && unzip && cd
#git clone https://gitee.com/Vivio2x/k8s-install.git -b v1.1.0
#cd k8s-install
wget https://gitee.com/Vivio2x/k8s-install/repository/archive/v1.1.0.zip
unzip  v1.1.0.zip &&  cd k8s-install-v1.1.0/
#安装
chmod +x k8s-install.sh && ./k8s-install.sh -osy
```

其它节点：加入集群。复制以下内容并**修改其中的`--master`**，在节点终端上执行。

```bash
#下载代码，如果安装git就使用git && cd, 否则使用wget && unzip && cd
#git clone https://gitee.com/Vivio2x/k8s-install.git -b v1.1.0
#cd k8s-install
wget https://gitee.com/Vivio2x/k8s-install/repository/archive/v1.1.0.zip
unzip  v1.1.0.zip &&  cd k8s-install-v1.1.0/
chmod +x k8s-install.sh && ./k8s-install.sh -osy --master=10.200.8.110
```

查看结果

```bash
root@k8s-001:~# kubectl get node
NAME      STATUS   ROLES                  AGE     VERSION
k8s-001   Ready    control-plane,master   10m     v1.21.11
k8s-002   Ready    <none>                 3m29s   v1.21.11
```



## 1.4. 安装了哪些东西
- cri: 默认为docker, 包含containerd
- kubernetes必备软件和命令行：kubeadm, kubelet, kubectl
- 常用的工具软件：
  - yq: yaml文件读取命令

# 2.安装说明-普通集群
## 2.0 主机网络设置
在线安装，则要求主机能联网，例如配置`/etc/resolv.conf`
```shell
nameserver 114.114.114.114
```

## 2.1 安装第一个节点
### 2.1.1   创建集群配置k8s.conf [可选]
创建配置文件`config/k8s.conf`, 文件格式参考`config/template/k8s.conf.tmpl`
（**本步骤可选，如果跳过，则集群配置全部使用默认值**(`config/template/k8s.conf.tmpl`定义)。）
- 例1,  创建k8s.conf, 并指定安装镜像源为阿里云和apiServer的名称为api.k8s.local
```yaml
cat > config/k8s.conf <<-EOF
K8S:
  remoteRegistry: registry.cn-hangzhou.aliyuncs.com/google_containers
  apiServerName: api.k8s.local
EOF
```
   **没有在config/k8s.conf中指定的配置，会自动设置为默认值**。

- 例2：使用模板创建k8s.conf
```bash
#1, 从模板复制
cp config/template/k8s.conf.tmpl config/k8s.conf

#2, 然后手动编辑文件
vim config/k8s.conf
```
`k8s.conf.tmpl`文件中包含所有的配置项, 见[k8s.conf.tmpl](#k8s.conf.tmpl)

### 2.1.2  创建节点配置node.yaml
创建配置文件config/node.yaml，文件格式参考`config/node.yaml.tmpl`
- 例1：创建node.yaml, 指定动作为创建新的集群，指定IP为192.168.60.101，
```bash
cat > config/node.yaml <<-EOF
NODE:
  hostname: k8s-001
  mode: create
  apiServerIp: 192.168.60.101
EOF
```
没有在config/node.yaml中指定的配置，会自动设置为默认值

- 例2：使用模板创建node.yaml
```bash
#1, 从模板复制
cp config/template/node.yaml.tmpl config/node.yaml

#2, 然后手动编辑文件
vim config/node.yaml
```
node.yaml.tmpl包含全部的配置项, 详细解释见 [node.yaml.tmpl](#node.yaml.tmpl)

### 2.1.3  安装
如果没有创建`k8s.conf`，则所有的配置都使用config/template/下模板中的默认配置

```bash
#在线安装, 安装完成后立即启动
chmod +x k8s-install.sh
./k8s-install.sh -osy
```
查看结果：
```shell
root@k8s-001:~/k8s/install# kubectl get node
NAME      STATUS   ROLES                  AGE   VERSION
k8s-001   Ready    control-plane,master   94s   v1.21.11
```

## 2.2 安装其它节点
### 2.2.1  创建k8s.conf
复制第一个节点的`/etc/vke/k8s.conf`到`config/k8s.conf`
```shell
scp root@192.168.60.101:/etc/vke/k8s.conf ./config/k8s.conf
```

### 2.2.2  创建node.yaml
mode 为join, 并指定apiServerIP
例, 加入到集群`192.168.60.101`中。

```bash
cat > config/node.yaml <<-EOF
NODE:
  hostname: k8s-002
  mode: join
  apiServerIp: 192.168.60.101
EOF
```

### 2.1.3  安装
```bash
#在线安装, 安装完成后立即启动
chmod +x k8s-install.sh
./k8s-install.sh -osy
```

查看结果
```bash
root@k8s-001:~/k8s/install# kubectl get node
NAME      STATUS   ROLES                  AGE     VERSION
k8s-001   Ready    control-plane,master   6m2s    v1.21.11
k8s-002   Ready    <none>                 5m10s   v1.21.11
```

# 3. 安装说明-高可用集群
## 3.0 节点IP规划

| ip | 用途 | 角色|
| :-- | :-- | :-- |
| 192.168.60.101 | k8s-001 ip | master,controlPlane |
| 192.168.60.102 | k8s-002 ip | master,controlPlane |
| 192.168.60.103 | k8s-003 ip | master,controlPlane |
| 192.168.60.100 | 虚拟vip | apiServerIP |

## 3.1 第一个节点

### 3.1.1 配置k8s.conf

设置ha为true
```yaml
cat > config/k8s.conf <<-EOF
K8S:
  ha: true
EOF
```
### 3.1.2 配置node.yaml

配置虚拟IP，并指定apiServerIp为虚拟IP `192.168.60.100`
```yaml
cat > config/node.yaml <<-EOF
NODE:
  hostname: k8s-001
  mode: create
  apiServerIp: 192.168.60.100
  vip: 192.168.60.100
EOF
```
### 3.1.3 安装

在线安装，`-o`

```shell
chmod +x k8s-install.sh
./k8s-install.sh -osy
```
离线安装， 则没有`-o`选项

```bash
chmod +x k8s-install.sh
./k8s-install.sh -sy
```

参数说明：`o`-使用apt或者yum安装，`s`-安装完成后立即启动k8s,`y`-安装过程中所有交互自动输入"yes"
查看结果, 多一个kube-vip-xx的pod, 并且mgmt接口多了一个IP，192.168.60.100

```shell
root@k8s-001:~/k8s/install# kubectl get node
NAME      STATUS   ROLES                  AGE    VERSION
k8s-001   Ready    control-plane,master   4m1s   v1.21.11
root@k8s-001:~/k8s/install# kubectl get pod -A
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-5d4b78db86-vsq79   1/1     Running   0          5m19s
kube-system   calico-node-cgv5k                          1/1     Running   0          5m18s
kube-system   coredns-6f6b8cc4f6-5v6h6                   1/1     Running   0          5m19s
kube-system   coredns-6f6b8cc4f6-6jflt                   1/1     Running   0          5m18s
kube-system   etcd-k8s-001                               1/1     Running   4          5m23s
kube-system   kube-apiserver-k8s-001                     1/1     Running   4          5m23s
kube-system   kube-controller-manager-k8s-001            1/1     Running   4          5m23s
kube-system   kube-proxy-8vw8x                           1/1     Running   0          5m18s
kube-system   kube-scheduler-k8s-001                     1/1     Running   5          5m23s
kube-system   kube-vip-k8s-001                           1/1     Running   0          5m23s
root@k8s-001:~/k8s/install# ip addr | more
...
7: br-int: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 2a:6e:ae:82:07:41 brd ff:ff:ff:ff:ff:ff
8: mgmt: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether ea:6e:54:66:a9:47 brd ff:ff:ff:ff:ff:ff
    inet 192.168.60.101/24 brd 192.168.60.255 scope global mgmt
       valid_lft forever preferred_lft forever
    inet 192.168.60.100/32 scope global mgmt

```

## 3.2 加入第二个controlplane节点
### 3.2.1 配置k8s.conf
设置ha为true
```yaml
cat > config/k8s.conf <<-EOF
K8S:
  ha: true
EOF
```
### 3.2.3 配置node.yaml
编辑`config/node.yaml`如下, mode为`join`, clusterRole为`controlPlane`
```yaml
cat > config/node.yaml <<-EOF
NODE:
  hostname: k8s-002
  mode: join
  clusterRole: controlPlane
  apiServerIp: 192.168.60.100
  vip: 192.168.60.100
EOF
```

### 3.2.3 安装

在线安装

```bash
chmod +x k8s-install.sh
./k8s-install.sh -osy
```
离线安装，没有`-o`选项

```bash
chmod +x k8s-install.sh
./k8s-install.sh -sy
```

查看结果

```bash
root@k8s-001:~/k8s/install# kubectl get node
NAME      STATUS   ROLES                  AGE     VERSION
k8s-001   Ready    control-plane,master   5m12s   v1.21.11
k8s-002   Ready    control-plane,master   66s     v1.21.11
```

## 3.3 加入第三个controlplane节点
### 3.3.1 配置k8s.conf
设置ha为true
```yaml
cat > config/k8s.conf <<-EOF
K8S:
  ha: true
EOF
```

### 3.3.3 配置node.yaml
编辑`config/node.yaml`如下, mode为`join`, clusterRole为`controlPlane`
```yaml
cat > config/node.yaml <<-EOF
NODE:
  hostname: k8s-003
  mode: join
  clusterRole: controlPlane
  apiServerIp: 192.168.60.100
  vip: 192.168.60.100
EOF
```

### 3.3.3 安装

在线安装

```bash
chmod +x k8s-install.sh
./k8s-install.sh -osy
```
离线安装，没有`-o`选项

```bash
chmod +x k8s-install.sh
./k8s-install.sh -sy
```



查看结果

```bash
root@k8s-001:~/k8s/install# kubectl get node
NAME      STATUS   ROLES                  AGE   VERSION
k8s-001   Ready    control-plane,master   62m   v1.21.11
k8s-002   Ready    control-plane,master   58m   v1.21.11
k8s-003   Ready    control-plane,master   49m   v1.21.11

```

# 4. 卸载
```shell
k8s-rm
```



# 5. 离线安装

## 5.0 编辑下载的URL

编辑 `config/template/download.yaml.tmpl`文件，修改相关的下载链接。

例如，kubeadm的二进制文件

如果你有更快的下载地址，请将`URL.kubeadm.bin`的内容`https://dl.k8s.io/release/v${PKGS_kubeadm:-1.21.11}/bin/linux/${SYS_archAlias}/kubeadm`替换为你自己的地址。

如果没有修改，则默认地址基本上都是官方地址。

```yaml
URL: 
  #github代理
  githubProxy: "https://ghproxy.com/"
  imageProxy: "m.daocloud.io"
  packageMirror: "https://mirrors.aliyun.com"
  docker:
    ...

  kubernetes:
    latestVersion: https://dl.k8s.io/release/stable.txt
  kubeadm:
    bin: "https://dl.k8s.io/release/v${PKGS_kubeadm:-1.21.11}/bin/linux/${SYS_archAlias}/kubeadm"
    conf: https://raw.githubusercontent.com/kubernetes/release/v0.15.1/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf
    ...
```



## 5.1 构造离线安装包

```bash
chmod +x k8s-install.sh
# -d 从官网下载安装的二进制文件和镜像文件，只下载不安装
./k8s-install.sh -d

# 压缩成tgz
cd ../
tar zcvf k8s-install.tgz k8s-install/
```

## 5.2 解压安装包

将安装包拷贝到目标主机上

```bash
tar zxvf k8s-install.tgz
cd k8s-install
chmod +x k8s-install.sh
```

## 5.3 安装

安装过程同`3. 安装说明-高可用集群`

或者使用

区别在于，执行安装命令时，不能使用`-o`(在线安装)选项

```bash
./k8s-install.sh -sy
```

# 6. 命令行方式安装
## 6.1 第一个节点

前提条件：确保主机上没有`192.168.60.100`这个地址，如果有，并且是之前安装的虚拟地址，则卸载之前的内容，重启主机。

```bash
chmod +x k8s-install.sh
./k8s-install.sh -sy --ha --vip=192.168.60.100
```

- 在线安装`-osy`, 离线安装`-sy`

- `--ha`: 启用高可用

- `--vip=192.168.60.100`: 设置虚拟IP为192.168.60.100

## 6.2 其它节点

```bash
chmod +x k8s-install.sh
./k8s-install.sh -sy --ha --vip=192.168.60.100 --master=192.168.60.100 --role=controlPlane
```

- 在线安装`-osy`, 离线安装`-sy`

- `--ha`: 启用高可用

- `--vip=192.168.60.100`: 设置虚拟IP为192.168.60.100

- `--master=192.168.60.100`: 设置要加入的集群

- `--role=controlPlane` 可以是`controlPlane`, 也可以是`worker`。



# 6. 配置文件说明

## 6.1 安装组件定义 package-1.21.11.yaml

- 配置路径： `/etc/vke/package-1.21.11.yaml`
- 配置内容如下
```yaml
# SYS_arch: x86_64 aarch64
# SYS_archAlias: amd64 arm64

SYS:
  #系统架构，安装程序自动识别填充
  arch: x86_64
  archAlias: amd64

# 1，先后顺序即安装顺序，key对应于components下的目录
#   例如PKGS.yq对应于components/yq目录
# 2， 冒号后为版本号，如果没有指定，则安装默认版本
PKGS:
  # 辅助工具和命令
  yq: 4.34.1
  #jq: 1.6

  # chart包管理
  #helm: 3.9.4

  # 容器运行时，cri
  # cri版本推荐请查看：https://github.com/kubernetes/kubernetes/blob/v1.22.0/build/dependencies.yaml
  # containerd版本没有要求
  docker: 20.10.24

  # kubernetes组件，包含kubeadm/kubelet/kubectl统一安装
  kubeadm: 1.21.11
  # kube-vip 高可用
  kube-vip: 0.5.5
  ethtool:
  ebtables:
  conntrack:

CHARTS:
  # 网络接口cni
  calico: 3.24.1
```

## 6.2 集群配置 k8s.conf

主要为集群的整体配置，所有节点上的k8s.conf都一样。不经常修改，可以直接使用默认值。
- 配置路径：`/etc/vke/k8s.conf`
  如果安装参数中`startNow=false`, 则可以在安装后，进行修改k8s.conf，然后`k8s-start`
- 模板：安装包中`config/template/k8s.conf.tmpl`
```yaml
#本文件将使用脚本进行读取与编辑，限制条件如下
# 1，所有第二层的key不能同名
# 2，注释不能写在正文后方
K8S:
  vkeConfigPath: /etc/vke
  vkeLibPath: /usr/lib/vke
  vkeVarPath: /var/lib/vke

  #是否开启匿名访问
  version: 1.21.11
  anonymousAuth: true

  #容器运行时配置, 默认为docker
  # criSocket和sandbox可以不填，由系统根据上文中的cri选项，自动补全
  #cri: containerd
  #criSocket: /run/containerd/containerd.sock
  cri: docker
  criSocket:
  sandbox:

  #CNI网络接口配置, 默认为calico
  cni: calico

  #API server配置
  apiServerName: api.k8s.local
  apiServerPort: 6443

  #cgroup driver, 例如systemd，可以不填，由系统自动识别
  cgroupDriver:

  #镜像仓库配置
  #安装基础的kubernetes时，使用remoteRegistry。
  kubeRegistry: registry.cn-hangzhou.aliyuncs.com/google_containers

  #时区设置
  timezone: Asia/Shanghai

  #是否为高可用集群
  ha: false

  #vip配置
  #vipEnable: true, false
  vipEnable: false
  #mode: 1, kube-vip 2, keepalived
  vipMode: kube-vip

  #network网络配置
  serviceCidr: 100.105.0.0/16
  clusterCidr: 100.101.0.0/16
  clusterDns: 100.105.0.10
```

## 6.3 节点配置 node.yaml

主要为与节点强相关的配置，经常需要修改
- 配置路径：`/etc/vke/node.yaml`
  如果安装参数中`startNow=false`, 则可以在安装后，进行修改node.yaml，然后`k8s-start`
- 模板：安装包中`config/template/node.yaml.tmpl`
```yaml
NODE:
  #节点名称，如果没有指定，则使用默认的主机名$(hostname)
  #hostname: k8s001
  hostname:

  #集群IP，[必填]
  apiServerIp:
  #模式: create-创建新集群，join-加入已有集群[必填]
  mode:
  #vip选填
  vip:
  #vip所在的接口，如果没有指定，则默认为internalInterface
  vipInterface:

  #集群角色: controlePlane, worker。如果没有指定，则默认为worker
  #如果mode=create,则强制设置为controlPlane，
  #如果mode=join， 则可选择设置为controlPlane或者worker
  #clusterRole: controlPlane
  clusterRole:

  #主机配置：在网络中公开的信息
  #管理接口，如果没有指定，则默认为default路由对应的接口IP, 格式为a.b.c.d/m
  #例：mgmtInterface:
  mgmtInterface:
  #主机IP，如果没有指定，则默认为default路由对应的接口IP,,  格式为a.b.c.d/m
  #mgmtIpaddress: 10.200.8.110/24
  mgmtIpaddress:

  #节点配置：在集群内公开的信息
  #节点内部接口，如果没有指定，则默认与mgmtInterface相同
  #internalInterface: ens33
  internalInterface:
  #节点在集群内部IP，如果没有指定，则默认与mgmtIpaddress相同,  格式为a.b.c.d/m
  #internalIP: 100.200.0.1/24
  internalIP:

  maxPods: 110
  #imageRegistryIP:
```

## 6.4 集群初始化文件 init-k8s.yaml

是为使用kubeadm init --config时需要的配置文件。
- 配置路径：`/etc/vke/init-k8s.yaml`
  如果安装参数中`startNow=false`, 则可以在安装后，进行修改init-k8s.yaml，然后`k8s-start`
- 模板：安装包中`config/template/init-k8s.yaml.tmpl`
  模板中的变量与`k8s.conf`, `node.yaml`中的定义相对应，例如`NODE_internalIP`即为`node.yaml`中的`NODE.internalIP`
```yaml
##\本文使用方法：假设本文件的绝对路径为/a/init-k8s.yaml.tmpl
##\1，定义文件中所有的变量，或者修改为默认值
##\2, eval "cat <<EOF
##\$(< /a/init-k8s.yaml.tmpl)
##\EOF"  > /a/init-k8s.yaml
##\3, kubeadm init --config=/a/init-k8s.yaml
##
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: ${NODE_internalIP%/*}
  bindPort: 6443
nodeRegistration:
  criSocket: ${K8S_criSocket}
  #imagePullPolicy: IfNotPresent
  taints: null
bootstrapTokens:
- groups:
  - system:bootstrappers:kubeadm:default-node-token
  token: abcdef.0123456789abcdef
  ttl: 24h0m0s
  usages:
  - signing
  - authentication
---
apiVersion: kubeadm.k8s.io/v1beta2
imageRepository: ${K8S_kubeRegistry}
controlPlaneEndpoint: "${K8S_apiServerName}:6443"

## /etc/kubernetes/manifests/kube-apiserver.yaml, extraArgs与启动参数--key=value对应
apiServer:
  extraArgs:
    ## pod --enable-admission-plugins=NodeRestriction,PodSecurityPolicy
    enable-admission-plugins: NodeRestriction
    authorization-mode: Node,RBAC
    anonymous-auth: "true"
    ## 确保 kubelet 使用安全的方式连接到 Kubernetes API Server
    #kubelet-certificate-authority: /etc/kubernetes/pki/ca.crt
    ## 关闭API server的性能分析功能，以加强集群的安全性和防护能力，以及避免性能监控信息被恶意利用或泄露
    profiling: "false"
    ## 审计日志
    #audit-log-path: /var/log/kubernetes/audit/audit.log
    #audit-log-maxage: "30"
    #audit-log-maxbackup: "10"
    #audit-log-maxsize: "100"
    #audit-policy-file: /etc/kubernetes/audit/audit-policy.yaml
  #extraVolumes:
    #- name: audit-log
    #  hostPath: /var/log/kubernetes/audit/
    #  mountPath: /var/log/kubernetes/audit/
    #  pathType: DirectoryOrCreate
    #- name: audit
    #  hostPath: /etc/kubernetes/audit/audit-policy.yaml
    #  mountPath: /etc/kubernetes/audit/audit-policy.yaml
    #  pathType: File
  timeoutForControlPlane: 4m0s
  certSANs:
  - ${K8S_apiServerName}
kind: ClusterConfiguration
kubernetesVersion: ${K8S_version}
networking:
  dnsDomain: cluster.local
  serviceSubnet: ${K8S_serviceCidr} #3 根据实际情况配置, 可以不修改
  podSubnet: ${K8S_clusterCidr} #4 根据实际情况配置, 可以不修改

## etcd 配置
etcd:
  local:
    dataDir: /var/lib/etcd # etcd的数据目录

## /etc/kubernetes/manifests/kube-controller-manager.yaml， extraArgs与启动参数--key=value对应
controllerManager:
  extraArgs:
    profiling: "false"
    bind-address: "127.0.0.1"

## /etc/kubernetes/manifests/kube-scheduler.yaml， extraArgs与启动参数--key=value对应
scheduler:
  extraArgs:
    profiling: "false"
    bind-address: "127.0.0.1"
---
#https://kubernetes.io/zh-cn/docs/reference/config-api/kubelet-config.v1beta1/
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: ${K8S_cgroupDriver}
kind: KubeletConfiguration
clusterDNS:
- ${K8S_clusterDns}
## true:发现内核参数与预期不符时出错退出。false: kubelet会尝试更改内核参数以满足其预期
protectKernelDefaults: true
---
apiVersion: kubeproxy.config.k8s.io/v1alpha1
kind: KubeProxyConfiguration
mode: "ipvs" #5 根据需求设置
metricsBindAddress: "0.0.0.0"
```
## 6.5 软件下载URL

所有软件下载的链接，尽量为官方链接
- 配置路径：`安装包/config/download.yaml`
- 模板: `安装包/config/template/download.yaml.tmpl`模板中的变量与`package-x.x.x.yaml`定义对应
```yaml
#软件下载链接
# SYS_arch: x86_64 aarch64
# SYS_archAlias: amd64 arm64
# PKGS_xxx表示软件xxx的版本号

# comment
URL:
  #github代理
  githubProxy: "https://ghproxy.com/"
  packageMirror: "https://mirrors.aliyun.com"
  docker:
    #docker image镜像加速站
    imageMirror: https://9zufkov1.mirror.aliyuncs.com
    bin: https://download.docker.com/linux/static/stable/${SYS_arch}/docker-${PKGS_docker:-20.10.24}.tgz
    service: https://raw.githubusercontent.com/moby/moby/master/contrib/init/systemd/docker.service
    socket: https://raw.githubusercontent.com/moby/moby/master/contrib/init/systemd/docker.socket
    installScripts:

  kubernetes:
    latestVersion: https://dl.k8s.io/release/stable.txt
  kubeadm:
    bin: "https://dl.k8s.io/release/v${PKGS_kubeadm:-1.21.11}/bin/linux/${SYS_archAlias}/kubeadm"
    conf: https://raw.githubusercontent.com/kubernetes/release/v0.15.1/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf
  kubelet:
    bin: "https://dl.k8s.io/release/v${PKGS_kubeadm:-1.21.11}/bin/linux/${SYS_archAlias}/kubelet"
    service: https://raw.githubusercontent.com/kubernetes/release/v0.15.1/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service
  kubectl:
    bin: "https://dl.k8s.io/release/v${PKGS_kubeadm:-1.21.11}/bin/linux/${SYS_archAlias}/kubectl"

  helm:
    #https://github.com/helm/helm/releases
    #bin: https://gitee.com/Vivio2x/helm/releases/download/v${PKGS_helm:-3.9.4}/helm-v${PKGS_helm:3.9.4}-linux-${SYS_archAlias}.tar.gz
    bin: https://get.helm.sh/helm-v${PKG_helm:-3.9.4}-linux-amd64.tar.gz
    installScripts: https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3

  yq:
    #bin: https://gitee.com/Vivio2x/yq/releases/download/v${PKGS_yq:-4.33.3}/yq_linux_${SYS_archAlias}
    bin: https://github.com/mikefarah/yq/releases/download/v${PKGS_yq:-4.33.3}/yq_linux_${SYS_archAlias}

  jq:
    #bin: https://gitee.com/Vivio2x/jq/releases/download/jq-${PKGS_jq:-1.6}/jq-linux64
    bin: https://github.com/stedolan/jq/releases/download/jq-${PKGS_jq:-1.6}/jq-linux64

  calico:
    manifest:
      - https://raw.githubusercontent.com/projectcalico/calico/v${PKGS_calico:-3.24.1}/manifests/calico.yaml
    chart: https://github.com/projectcalico/calico/releases/download/v${PKGS_calico:-3.24.1}/tigera-operator-v${PKGS_calico:-3.24.1}.tgz

```

# 7. 常见问题说明

## 7.1 二进制文件损坏

```shell
+ yq -i '.insecure-registries += [""] | .insecure-registries |= unique' -o json /etc/docker/daemon.json
/root/code/vv-note/k8s/src/install/scripts/k8s-start.sh: line 12: 55598 Segmentation fault      yq -i "$yqPath += $new | $yqPath |= unique" -o json $file
+ yq_add_array /etc/docker/daemon.json .exec-opts native.cgroupdriver=
root@k8s-master:~/code/vv-note/k8s/src/install#  yq --version
Segmentation fault

```

解决方法：删除该软件，重启

```shell
root@k8s-master:~/code/vv-note/k8s/src/install# command -v yq
/usr/local/bin/yq
root@k8s-master:~/code/vv-note/k8s/src/install# rm /usr/local/bin/yq
root@k8s-master:~/code/vv-note/k8s/src/install# reboot
```

## 7.2 其它节点安装太晚，加入集群失败
原因: kubeadm-certs有效期到了，需要重新生成
解决：在第一个节点上，执行`k8s-joincmd`


## 7.3 apt update失败
例如出现以下内容
```bash
Failed to fetch https://packages.gitlab.com/gitlab/gitlab-ce/ubuntu/dists/bionic/InRelease  Could not connect to 
```
删除/etc/apt/source.list.d/gitlab*等内容，apt update时，不更新gitlab
```bash
rm /etc/apt/sources.list.d/gitlab* -rf
```

