#!/bin/bash

#查看操作系统, OS=centos, ubuntu, ...
OS=$(cat /etc/os-release | grep ^ID= | awk -F "=" '{print $2}' | sed s/\"//g)

pblue() {
  echo -e "\033[34m $1 \033[0m"
}
stop_and_disable_service() {
    service=$1
    pblue "disable $service"
    if systemctl is-active $service -q >/dev/null 2>&1; then
       bash -xc "systemctl stop $service"
    fi
    if systemctl is-enabled $service -q >/dev/null 2>&1 ; then
       bash -xc "systemctl disable $service"
    fi
    echo "$(systemctl status $service 2>/dev/null)" | grep -E "Loaded|Active"
}

#创建/etc/sysctl.d/k8s.conf
create_system_conf() {
    rm -rf /etc/sysctl.d/k8s.conf
    touch /etc/sysctl.d/k8s.conf

    rm -rf /etc/modules-load.d/k8s.conf
    touch /etc/modules-load.d/k8s.conf
}

#禁用firewalld centos
disable_firewall_centos() {
    stop_and_disable_service firewalld
}

#禁用firewalld ubuntu
disable_firewall_ubuntu() {
    if which ufw ; then
        ufw disable
        ufw status
    fi
    stop_and_disable_service ufw
}

disable_firewall() {
    pblue "disable firewall"
    case $OS in
        "centos") disable_firewall_centos ;;
        "ubuntu") disable_firewall_ubuntu ;;
    esac
}


#关闭networkmanager
disable_networkManager() {
    stop_and_disable_service NetworkManager
}


#禁用swap, 查找到swap，并将行首替换成
disable_swap() {
    pblue "disable swap"
    sed -i '/swap/s/^/#/g' /etc/fstab   #永久禁用
    swapoff -a                          #临时关闭

    echo 0 > /proc/sys/vm/swappiness         #修改swappiness 内核参数
    echo 'vm.swappiness=0' >> /etc/sysctl.d/k8s.conf

    # sysctl --system #加载配置，后续set_kernel_args会统一执行sysctl --system          
    free -m              #查看结果               
    echo "-------------------------------disable swap successfully"
}

#关闭selinux
disable_selinux() {
    pblue "disable selinux"
    if [ -f /etc/selinux/config ]; then
        #临时关闭
        setenforce 0
        #永久关闭，重启生效
        sed -i "/^SELINUX=/c\SELINUX=disabled" /etc/selinux/config
         #查看结果
        cat /etc/selinux/config | grep SELINUX=
        /usr/sbin/sestatus
        getenforce
    fi 
  echo "-------------------------------disable selinux successfully"
}


#设置内核转发参数
set_kernel_args() {
    pblue "set kernel args:  /etc/sysctl.d/k8s.conf"
    cat >> /etc/sysctl.d/k8s.conf << EOF
# 网络转发
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.ipv6.conf.all.disable_ipv6 = 1

# CIS 加固
vm.overcommit_memory = 1
vm.panic_on_oom = 0
kernel.panic_on_oops = 1
kernel.panic = 10
EOF
   # 生效
    sysctl --system
    echo "-------------------------------set kernel args successfully"
}

#加载内核模块
set_kernel_modules() {
    pblue "set kernel modules: /etc/modules-load.d/k8s.conf"
    cat >> /etc/modules-load.d/k8s.conf << EOF
#------kube-proxy ipvs------------
$(ls /lib/modules/$(uname -r)/kernel/net/netfilter/ipvs | grep .ko | grep -o ^[^.]*)
# use nf_conntrack instead of nf_conntrack_ipv4 for Linux kernel 4.19 and later
nf_conntrack

#------br_netfilter---------------
overlay
br_netfilter

#------for Calico----------------
ip_set
ip_tables
ip6_tables
ipt_REJECT
ipt_rpfilter
ipt_set
nf_conntrack_netlink
#nf_conntrack_proto_sctp
sctp
xt_addrtype
xt_comment
xt_conntrack
#xt_icmp
#xt_icmp6
xt_ipvs
xt_mark
xt_multiport
#xt_rpfilter
xt_sctp
xt_set
xt_u32
ipip
EOF
   # cat /etc/modules-load.d/k8s.conf | grep -vE "#|^$"
   systemctl enable --now systemd-modules-load.service
   systemctl restart systemd-modules-load.service

}

#补充ubuntu专用
set_dns() {
  systemctl restart systemd-resolved
  systemctl stop systemd-resolved
  systemctl disable systemd-resolved
  echo "nameserver 114.114.114.114" > /etc/resolv.conf
}
#清空iptables规则 
flush_iptables() {
  pblue "flush iptables"
  set -x
  iptables -F
  iptables -X 
  iptables -F -t nat
  iptables -X -t nat 
  iptables -t mangle -F
  iptables -t mangle -X
  iptables -P INPUT ACCEPT
  iptables -P FORWARD ACCEPT
  iptables -P OUTPUT ACCEPT
  set +x
}


create_system_conf      #1, 创建/etc/sysctl.d/k8s.conf,/etc/modules-load.d/k8s.conf
disable_firewall        #2, 禁用firewalld
disable_networkManager  #3, 禁用networkManager
disable_swap            #4, 禁用swap
disable_selinux         #5, 禁用selinux
set_kernel_args         #6, 设置内核参数
set_kernel_modules      #7, 加载内核模块
flush_iptables          #8, 清空iptables规则
