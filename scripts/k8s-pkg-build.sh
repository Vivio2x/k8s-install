#!/bin/bash
TOP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )"
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

SUPER_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../.." &> /dev/null && pwd )"
installScripts=${installScripts:-"$SUPER_DIR/.pkgsInstall"}
K8S_VERSION=${K8S_VERSION:-1.21.11}

# 默认情况下，当前目录中会有一个utils.sh, 但是shell/src/utils.sh中的文件是最标准的。
if [ -f $SUPER_DIR/shell/src/utils.sh ]; then
    cp -rf $SUPER_DIR/shell/src/utils.sh ./
fi
source $CUR_DIR/utils.sh

# copy apps
mkdir -p $TOP_DIR/components
pkgs=$(yq '.PKGS|keys|.[]' $TOP_DIR/config/package-${K8S_VERSION}.yaml)
for pkg in $pkgs; do
    install_sh=$(cat $installScripts | grep -E "/${pkg}-install.sh")
	src_dir=${install_sh%/*}
	dst_dir=$TOP_DIR/components/${pkg}
	rm -rf $dst_dir
	cp -rf $src_dir $dst_dir
done

mkdir -p $TOP_DIR/components/charts
charts=$(yq '.CHARTS|keys|.[]' $TOP_DIR/config/package-${K8S_VERSION}.yaml)
for chart in $charts; do
    install_sh=$(cat $installScripts | grep -E "/${chart}-install.sh")
	src_dir=${install_sh%/*}
	dst_dir=$TOP_DIR/components/charts/${chart}
	rm -rf $dst_dir
	cp -rf $src_dir $dst_dir
done

find $TOP_DIR -name "*.sh" | xargs chmod +x

echo "The Kubernetes installation directory has been successfully built. Below are the instructions for usage.
========================================================================================= 
0.      cd $TOP_DIR
1. create an offline installation package:  (If no version is set, it defaults to 1.21.11)

       ./k8s-install.sh -fd [ --version=<version> ]

   create a tgz compressed package:  
   
       cd ../ && tar zcvf k8s-install.tgz install/
	   
2. Install the offline installation package: (If no version is set, it defaults to 1.21.11)

       ./k8s-install.sh -f [ --version=<version> ]
	   
3. install Kubernetes using apt or yum : (If no version is set, it defaults to 1.21.11)

       ./k8s-install.sh -of [ --version=<version> ]
	   
4. more help info :
       bash $TOP_DIR/k8s-install.sh --help
"







