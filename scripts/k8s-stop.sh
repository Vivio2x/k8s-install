#!/bin/bash

set -x
kubeadm reset -f

rm -rf /var/lib/etcd
rm -rf /var/lib/kubelet
rm -rf /var/lib/kube-proxy
rm -rf /etc/kubernetes
rm -rf /etc/cni/net.d
rm -rf /var/lib/k8s
rm -rf $HOME/.kube


