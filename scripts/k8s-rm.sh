#!/bin/bash
K8S_vkeConfigPath=${K8S_vkeConfigPath:-/etc/vke}

set -x
kubeadm reset -f > /dev/null 2>&1
systemctl stop kubelet
rm -rf /var/lib/etcd
rm -rf /var/lib/kubelet
rm -rf /var/lib/kube-proxy
rm -rf /etc/kubernetes
rm -rf /etc/cni/net.d
rm -rf $HOME/.kube
rm -rf /var/lib/vke 
set +x

PACKAGE_MANAGER=$((command -v apt-get > /dev/null && echo apt-get) \
    || (command -v yum > /dev/null && echo yum) \
    || (echo "Neither apt-get nor yum is installed, please install one of them first." && exit 1))
if [ "$PACKAGE_MANAGER" == "apt-get" ]; then
    pkgs=$(dpkg -l | grep -E 'kubeadm|kubelet|kubectl' | awk '{print $2}')
    if [ -n "$pkgs" ] ; then
        set -x
        apt-get remove -y $pkgs > /dev/null 2>&1
        apt-get autoremove -y > /dev/null
        apt-get autoclean -y > /dev/null
        set +x
    fi
elif [ "$PACKAGE_MANAGER" == "yum" ]; then
    pkgs=$(rpm -qa | grep -E 'kubeadm|kubelet|kubectl' | awk '{print $1}')
    if [ -n "$pkgs" ] ; then
        set -x
        yum remove -y $pkgs > /dev/null 2>&1
        yum autoremove -y > /dev/null
        yum clean all > /dev/null
        set +x
    fi
fi
set -x
rm -rf /usr/local/bin/{kubeadm,kubelet,kubectl}
rm -rf /usr/bin/{kubeadm,kubelet,kubectl}

rm -rf /etc/systemd/system/kubelet.service*
rm -rf /lib/systemd/system/kubelet.service*

rm -rf $K8S_vkeConfigPath -rf


rm -rf /usr/local/bin/k8s-start
rm -rf /usr/local/bin/k8s-stop
rm -rf /usr/local/bin/k8s-rm





